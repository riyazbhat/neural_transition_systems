#!/usr/bin/python -*- coding: utf-8 -*-

import re
import os
import sys
import copy
import random
import timeit
import cPickle

import argparse
import numpy as np
from collections import Counter, namedtuple as ntp, defaultdict as dfd

from algorithms.swap import Swap
from algorithms.arc_standard import ArcStandard

from utils.inorder_traversal import * #get_in_order(graph) without dummies

random.seed(100)
np.random.seed(100)


class Configuration(object):
    def __init__(self, nodes=[]):
        self.stack = list()
	self.queue = range(len(nodes))[1:]
	self.score = 0.0
        self.b0 = 1
	self.nodes = [nodes[0]]+get_in_order(nodes[1:-1])+[nodes[-1]]
	for tnode in range(1, len(self.nodes[1:-1])+1):
	    tnodeparent = self.nodes[self.nodes[tnode].parent]
            self.nodes[tnodeparent.id] = self.nodes[tnodeparent.id]._replace(children=tnodeparent.children+[tnode])

def MPC(node, parent, tree):
	if parent == -1:
	    return node
	else:
	    node, parent = tree[parent].id, tree[parent].pparent
	    return MPC(node, parent,tree)

def MPCs(graph):
    configuration = Configuration(graph)
    while not nnparser.inFinalState(configuration):
        goldTransitionFunc, goldLabel = nnparser.predict(configuration)
        goldTransition = goldTransitionFunc.__name__
        goldTransitionFunc(configuration, goldLabel)
	
    for p in range(1,len(configuration.nodes[1:])+1):
	pN = configuration.nodes[p]
	pNParent = pN.pparent
	if pNParent == -1:
	    maxProjection = pN.id
	else:
	    maxProjection = MPC(pN,pNParent, configuration.nodes)
	configuration.nodes[p] = configuration.nodes[p]._replace(
					pdrel="__PAD__",
					inorder=-1,
					left=[None],
					right=[None],
					children=[],
					mpc=maxProjection)
    configuration.nodes[-1] = configuration.nodes[-1]._replace(inorder=p)
    #for nG in configuration.nodes[1:-1]:print nG.id,nG.pparent,nG.mpc
    return [cNode._replace(pparent=-1) for cNode in configuration.nodes]

def Train(sentence, rmaps, itercount, dynamic):
    configuration = Configuration(sentence)

    #for nnn in configuration.nodes[1:]:
    #	print "\t\t".join((str(nnn.id),nnn.form,'h='+str(nnn.parent),'o='+str(nnn.inorder),'m='+str(nnn.mpc)))
    totalError = 0
    while not nnparser.inFinalState(configuration):
	#print configuration.queue
	#print configuration.nodes[configuration.b0].form
	##NOTE <Static Oracle>
    	goldTransitionFunc, goldLabel = nnparser.predict(configuration)
	goldTransition = goldTransitionFunc.__name__
	_ = rmaps[(goldTransition, goldLabel)]
    	goldTransitionFunc(configuration, goldLabel)
	##NOTE decoding the code
	#si,sj,sp = None, None , None
	#if len(configuration.stack[-2:]) > 1:
	#    si,sj,sp = configuration.nodes[configuration.stack[-2]].inorder, \
		#configuration.nodes[configuration.stack[-1]].inorder, configuration.nodes[configuration.stack[-2]].parent
	#print goldTransition, goldLabel, configuration.stack,configuration.queue,configuration.b0#, si,sj, sp
	##</Static Oracle>
    #print
    for nnn in configuration.nodes[1:-1]:
	assert (nnn.parent == nnn.pparent) and (nnn.drel == nnn.pdrel)

    #assert nn == (2*len(sentence[1:]))-1

def nntraining(dataset=[], n_epochs=1, dynamic=True):
    rmaps = {v: k for k, v in tdmaps.iteritems()}
    for epoch in range(n_epochs):
    	np.random.shuffle(dataset)
    	gtotalError = gtotal = 0
    	for sentid, sentence in enumerate(dataset):
	    sentid += 1
	    csentence = copy.deepcopy(sentence)
	    #Train(csentence, rmaps, epoch+1,dynamic)
	    #'''
	    try:
	        Train(csentence, rmaps, epoch+1,dynamic)
	    except:
                for nnn in csentence[1:-1]:
                    print "\t".join((str(nnn.id),nnn.form,'_',nnn.tag,'_','_',str(nnn.parent),nnn.drel,'_','_'))
		print
		print
            #''' 

def projective(nodes):
    """Identifies if a tree is non-projective or not."""
    for leaf1 in nodes:
        v1,v2 = sorted([int(leaf1.id), int(leaf1.parent)])
        for leaf2 in nodes:
            v3, v4 = sorted([int(leaf2.id), int(leaf2.parent)])
            if leaf1.id == leaf2.id:continue
            if (v1 < v3 < v2) and (v4 > v2): return False
    return True

def depenencyGraph(sentence):
    """Representation for dependency trees"""
    leaf=ntp('leaf',['id','form','lemma','tag','ctag','features','parent','pparent','drel','pdrel','inorder','left','right','children','mpc'])
    PAD = leaf._make([-1,'__PAD__','__PAD__','__PAD__','__PAD__',dfd(str),-1,-1,'__PAD__','__PAD__',-1,[None],[None],[], -1])
    yield leaf._make([0, 'ROOT_F', 'ROOT_L', 'ROOT_P', 'ROOT_C', dfd(str), -1, -1, '__ROOT__', '__ROOT__',-1,PAD, [None],[], -1])

    sentLength = len(sentence.split("\n"))
    for node in sentence.split("\n"):
        id_,form,lemma,ctag,tag,features,parent,drel = node.split("\t")[:-2]
        node = leaf._make([int(id_),form,lemma,tag,ctag,features,int(parent),-1,drel,'__PAD__',-1,[None],[None],[], int(id_)])
        features = dfd(str)
        for feature in node.features.split("|"): 
            try:
                features[feature.split("-")[0]] = feature.split("-")[1]
            except IndexError: features[feature.split("-")[0]] = ''
        yield node._replace(features=features)
    yield leaf._make([0,'ROOT_F','ROOT_L','ROOT_P','ROOT_C',dfd(str),-1,-1,'__ROOT__','__ROOT__',sentLength+1,[None],[None],[],sentLength+1])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Neural Network Parser.", description="Beam search trainer based on SGD.")
    parser.add_argument('--train', metavar='train', dest="train_file", required=True, help="<train-file>")
    args = parser.parse_args()

    with open(args.train_file) as infptrain:
        inputGenTrain = re.finditer("(.*?)\n\n", infptrain.read(), re.S)

    train_sents = list()
    dlabels = set()
    tdlabels = set()
    plabels = set()
    cc = Counter()
    cc['<*>'] += 1
    cc['_UNK_'] += 1
    tdlabels.add(('SHIFT', None))
    tdlabels.add(('SWAP', None))
    transitions = {'SHIFT':0,'LEFTARC':1,'RIGHTARC':2,'SWAP':3}
    #transitions = {'SHIFT':0,'LEFTARC':1,'RIGHTARC':2}

    nnparser = ArcStandard() 
    for sentence in inputGenTrain:
        graph = list(depenencyGraph(sentence.group(1)))
        if not projective(graph[1:-1]):
	    graph = MPCs(graph)
	    '''
	    for nG in graph[1:-1]:
	    	assert nG.pdrel == "__PAD__" and nG.pparent == -1 and nG.inorder == -1 and \
				nG.left == [None] and nG.right == [None] and nG.children == []
            #'''
	roots = 0
        for pnode in graph[1:-1]:
             for c in pnode.form:
                 cc[c] += 1
	     plabels.add(pnode.tag)
	     dlabels.add(pnode.drel)
	     if pnode.parent == 0:
		 roots += 1
                 tdlabels.add(('LEFTARC', pnode.drel))
	     elif pnode.id < pnode.parent:
                 tdlabels.add(('LEFTARC', pnode.drel))
	     else:
                 tdlabels.add(('RIGHTARC', pnode.drel))
        if roots == 1: train_sents.append(graph)
    plabels.add('LS')
    dlabels.add("__ROOT__")
    dlabels.add("__PAD__")
    plabels.add("__PAD__")
    plabels.add("ROOT_P")
    tdmaps = dict(zip(range(len(tdlabels)), tdlabels))
    pmaps = dict(zip(plabels, range(len(plabels))))
    dmaps = dict(zip(dlabels, range(len(dlabels))))
    nnparser = Swap() 
    nntraining(dataset=train_sents)

#!/usr/bin/python

import re
import sys

from itertools import permutations
from collections import defaultdict as dfd

with open(sys.argv[1]) as inp:
	sentences = inp.read().split("\n\n")[:-1]
	
arguments = dfd()

for argument in ['k1', 'k2', 'k4', 'k4a', 'k3', 'k5', 'k7', 'k7t', 'k7p']:
	arguments[argument] = [0.,0.]

clauseOrder = dfd()

SOV = ['k1', 'k2', 'V']
for argumentt in permutations(('k1', 'k2', 'V')):
	clauseOrder[argumentt] = 0.0
arg_sent = dict(zip(arguments,[[] for _ in arguments]))


for sentence in sentences:
	argumentTuples = dfd()
	sent = []
	for node in sentence.split("\n"):
		idx,word,lemma,cpos,pos,features,head,drel,_,_ = node.split("\t")
		sent.append("\t".join([idx, word, pos, head, drel]))
		idx, head = map(int, [idx,head])
		chunkid = re.sub("[0-9]*", "", features.split("|")[7].split("-")[-1])
		if drel in ["k1", "k2"] and (pos == "CC" or chunkid == "VGF"):continue
		if drel in arguments:
			if idx < head:
				arguments[drel][0] += 1
			else:
				arguments[drel][-1] += 1
				arg_sent[drel].append(sentence)
		if drel in SOV:
			argumentTuples.setdefault(head, [])
			argumentTuples[head].append((idx, drel))

	sent = "\n".join(sent)
	for headx, argumentTuple in argumentTuples.iteritems():
		atuple = {headx:'V'}
		for at in argumentTuple:
			id_, rel = at
			atuple[id_] = rel

		sortedAtuple = sorted(atuple.items(), key=lambda x: x[0])	
		argumentTuple = tuple([xx[1] for xx in sortedAtuple])
		if argumentTuple == ('k2', 'V', 'k1'):
			#print sent
			#print
			pass
		if argumentTuple in clauseOrder:
			clauseOrder[argumentTuple] += 1

for key, value in arg_sent.iteritems():
	if value > 0:
		oversample = value*1000
		for sent_ in oversample[:1000]:
			print sent_
			print

exit()

for t in sorted(clauseOrder.items(), key=lambda x: x[1], reverse=True):
	args, count = t
	#if count < 3: continue
	print "%s :: %s" % (" ".join(args), count)

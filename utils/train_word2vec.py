
import sys
from gensim.models.word2vec import *

with open(sys.argv[1]) as rawfp:
	sentences = rawfp.readlines()

model = Word2Vec(sentences, size=80, window=5, min_count=1, workers=4)
model.save('trialembd.vec')

#!/usr/bin/env python

def is_projective(sent):
   proj=True
   edges = int()
   spans = set()
   for tok in sent.split("\n"):
      tok = tok.split("\t")
      s = tuple(sorted([int(tok[0]), int(tok[6])]))
      spans.add(s)
   for l,h in sorted(spans):
      for l1,h1 in sorted(spans):
         if (l,h)==(l1,h1): continue
         if l < l1 < h and h1 > h:
            #print "non proj:",(l,h),(l1,h1)
	    edges += 1
            proj = False
   return edges

if __name__ == "__main__":

	import sys
	sentences = file(sys.argv[1]).read().strip().split("\n\n")
	totalEdges = int()
	nonProjectiveEdges = float()
	nonProjectiveSentences = float()
	for idx, sentence in enumerate(sentences):
		if not sentence.strip():continue
		totalEdges += len(sentence.split("\n"))
		np = is_projective(sentence)
		nonProjectiveEdges += np
		if np > 0.: nonProjectiveSentences += 1
	
	totalSentences = idx
	nonProjectiveEdges = 100. * nonProjectiveEdges/totalEdges
	nonProjectiveSentences = 100. * nonProjectiveSentences/totalSentences
	
	print "Non-projective Sentences=%s while Non-projective Edges=%s" % (nonProjectiveSentences,nonProjectiveEdges)

#!/usr/bin/python -*- coding: utf-8 -*-

from __future__ import print_function

import re
import io
import os
import sys
import copy
import string
import timeit
import random
import pickle

import argparse
import numpy as np
from collections import Counter, namedtuple, defaultdict

import dynet as dy
from gensim.models.word2vec import *

from algorithms.swap_bohnet import Swap
from algorithms.arc_standard import ArcStandard

from utils.inorder_traversal import * #get_in_order(graph) without dummies

random.seed(37)
np.random.seed(37)

class NNParser(Swap):
    def __init__(self, wvec, tdmaps={}, pmaps={}, cmaps={}, dmaps={}, window=0, n_hidden=128, chars={}, c2i={}):
        self.model = dy.Model()
        self.trainer = dy.MomentumSGDTrainer(self.model, edecay=0.5)
        wvec = wvec
        self.tdmaps = tdmaps
        self.pmaps = pmaps
        self.cmaps = cmaps
        self.dmaps = dmaps
        self.n_out = len(tdmaps)
        self.n_tags = len(pmaps)
        self.n_chunks = len(cmaps)
        self.n_drels = len(dmaps)
        self.lstmdims = 128
        self.win = window
        self.nlexdims = 32#self.n_tags # dimension of no-lexical embeddings like POS, dep labels etc.
        self.n_hidden = 128
        self.nwords, self.w_dim = wvec.syn0.shape
        self.chars = chars
        self.c2i = c2i
        self.c_dim = 32

        self.punct_table = dict((ord(char), None) for char in string.punctuation)
        self.isurl = re.compile(r'[a-z][a-z][.][a-z][a-z]').search
        self.upunct = re.compile(r'[.,\\!@#$%^&\'*()_+={\[}\]|";:<>?`~/]')
        self.domains = set('''to cc tr ws ee mx hk ar il ps eu fm pt gz do mil mp cn ro tk in co hu cz ru br no gr 
                              es jp fi tv dk se be za int at nz us fr ie ch nl pl ca gov info it au de edu net org
                              com uk'''.split())

        # ndims: hidden x input
        self.pW1 = self.model.add_parameters((self.n_hidden, (self.lstmdims*2+self.nlexdims)*self.win))
        self.pb1 = self.model.add_parameters(self.n_hidden) # ndims: hidden units
        self.pW2 = self.model.add_parameters((self.n_out, self.n_hidden)) #ndims: output x hidden
        self.pb2 = self.model.add_parameters(self.n_out) # ndims: output

        #self.fwdRNN = dy.LSTMBuilder(1, self.w_dim*2+self.nlexdims, self.lstmdims, self.model) # layers, in-dim, out-dim, model
        #self.bwdRNN = dy.LSTMBuilder(1, self.w_dim*2+self.nlexdims, self.lstmdims, self.model)
        self.fwdRNN = dy.LSTMBuilder(1, self.w_dim*2, self.lstmdims, self.model) # layers, in-dim, out-dim, model
        self.bwdRNN = dy.LSTMBuilder(1, self.w_dim*2, self.lstmdims, self.model)
        self.bi_fwdRNN = dy.LSTMBuilder(1, self.lstmdims*2, self.lstmdims, self.model) # layers, in-dim, out-dim, model
        self.bi_bwdRNN = dy.LSTMBuilder(1, self.lstmdims*2, self.lstmdims, self.model)


        self.cfwdRNN = dy.LSTMBuilder(1, self.c_dim, int(self.w_dim/2), self.model)
        self.cbwdRNN = dy.LSTMBuilder(1, self.c_dim, int(self.w_dim/2), self.model)

        #self.pword2lstm = self.model.add_parameters((self.lstmdims*2, self.w_dim + self.nlexdims))
        self.pword2lstm = self.model.add_parameters((self.lstmdims*2, self.w_dim))
        self.pword2lstmbias = self.model.add_parameters(self.lstmdims*2)

        self.TAGS = self.model.add_lookup_parameters((self.n_tags+1, self.nlexdims))
        self.WORDS_LOOKUP = self.model.add_lookup_parameters((self.nwords+3, self.w_dim))
        self.CHARS_LOOKUP = self.model.add_lookup_parameters((len(chars), self.c_dim))

        self.w2i = {}
        for word, V in wvec.vocab.iteritems():
            self.w2i[word] = V.index+3
            self.WORDS_LOOKUP.init_row(V.index+3, wvec.syn0[V.index])

        if args.lmodel:
            self.model.load('%s.dy' %args.lmodel)

    def initialize_graph_nodes(self, train=False):
        self.W1 = dy.parameter(self.pW1)
        self.b1 = dy.parameter(self.pb1)
        self.W2 = dy.parameter(self.pW2)
        self.b2 = dy.parameter(self.pb2)

        ## APPLY DROPOUT
        if train:
            self.W1 = dy.dropout(self.W1, 0.25)
            self.b1 = dy.dropout(self.b1, 0.25)
            self.fwdRNN.set_dropout(0.25)
            self.bwdRNN.set_dropout(0.25)
            self.cfwdRNN.set_dropout(0.25)
            self.cbwdRNN.set_dropout(0.25)
            self.bi_fwdRNN.set_dropout(0.25)
            self.bi_bwdRNN.set_dropout(0.25)
        else:
            self.fwdRNN.disable_dropout()
            self.bwdRNN.disable_dropout()
            self.cfwdRNN.disable_dropout()
            self.cbwdRNN.disable_dropout()
            self.bi_fwdRNN.disable_dropout()
            self.bi_bwdRNN.disable_dropout()

        self.f_init = self.fwdRNN.initial_state()
        self.b_init = self.bwdRNN.initial_state()

        self.bi_f_init = self.bi_fwdRNN.initial_state()
        self.bi_b_init = self.bi_bwdRNN.initial_state()

        self.cf_init = self.cfwdRNN.initial_state()
        self.cb_init = self.cbwdRNN.initial_state()

        self.word2lstm = dy.parameter(self.pword2lstm)
        self.word2lstmbias = dy.parameter(self.pword2lstmbias)

    def get_w_index(self, w):
        idx = None
        is_url = False
        try:
            return self.w2i[w]
        except KeyError:
            pass
        #if w.translate(self.punct_table).isdigit():
        #    return self.w2i['N-U-M']
        if (w.startswith('http://') or w.startswith('https://') or w.startswith('www.')):
            is_url = True
        elif self.isurl(w):
            tokens = self.upunct.split(w)
            is_url = any(tk in self.domains for tk in tokens[1:])
        #if is_url:
        #    return self.w2i['U-R-L']
        return idx

    def get_char_embd(self, node):
        pad_char = self.c2i["<*>"]
        char_ids = [pad_char] + [self.c2i[c] if self.chars[c]>5 else self.c2i['_UNK_'] for c in node] + [pad_char]
        char_embs = [self.CHARS_LOOKUP[cid] for cid in char_ids]
        fw_exps = self.cf_init.transduce(char_embs)
        bw_exps = self.cb_init.transduce(reversed(char_embs))
        return dy.concatenate([ fw_exps[-1], bw_exps[-1] ])

    def get_linear_embd(self, sequence, test=True):
        linear_embds = list()
        for node in sequence:
            if node.form == "ROOT_F":
                form_embd = self.WORDS_LOOKUP[1]
            elif node.form == "__PAD__":
                form_embd = self.WORDS_LOOKUP[2]
            elif not test and random.random() < 0.1:
                form_embd = self.WORDS_LOOKUP[0]
            else:
                index = self.get_w_index(node.form)
                if index is not None:
                    form_embd = self.WORDS_LOOKUP[index]
                else:
                    form_embd = self.WORDS_LOOKUP[0]
            #tag_embd = self.TAGS[self.pmaps[node.tag]] if self.pmaps.get(node.tag, None) else self.TAGS[0] * 0.001
            char_embd = self.get_char_embd(node.form)
            #linear_embds.append(dy.concatenate([form_embd, char_embd, tag_embd]))
            linear_embds.append(dy.concatenate([form_embd, char_embd]))
        return linear_embds

    def basefeaturesStandard(self, nodes, stack, i):
	#NOTE Stack nodes
        s3 = nodes[stack[-4]] if stack[3:] else nodes[0].left#==PAD
        s2 = nodes[stack[-3]] if stack[2:] else nodes[0].left#==PAD
        s1 = nodes[stack[-2]] if stack[1:] else nodes[0].left#==PAD
        s0 = nodes[stack[-1]] if stack else nodes[0].left#==PAD

	#NOTE Buffer nodes
	n0 = nodes[ i ] if nodes[ i: ] else nodes[0].left#==PAD
	n0left = n0.left if i else [None]
	#n1 = nodes[i+1] if nodes[i+1:] else nodes[0].left#==PAD
	#n2 = nodes[i+2] if nodes[i+2:] else nodes[0].left#==PAD

	#NOTE Leftmost and Rightmost children of s2,s1,s0 and b0(only leftmost)
	s3l = nodes[s3.left [-1]] if s3.left [-1] != None else nodes[0].left#==PAD
	s3r = nodes[s3.right[-1]] if s3.right[-1] != None else nodes[0].left#==PAD
	s2l = nodes[s2.left [-1]] if s2.left [-1] != None else nodes[0].left#==PAD
	s2r = nodes[s2.right[-1]] if s2.right[-1] != None else nodes[0].left#==PAD
	s1l = nodes[s1.left [-1]] if s1.left [-1] != None else nodes[0].left#==PAD
	s1r = nodes[s1.right[-1]] if s1.right[-1] != None else nodes[0].left#==PAD
	s0l = nodes[s0.left [-1]] if s0.left [-1] != None else nodes[0].left#==PAD
	s0r = nodes[s0.right[-1]] if s0.right[-1] != None else nodes[0].left#==PAD
	n0l = nodes[n0left  [-1]] if n0left  [-1] != None else nodes[0].left#==PAD
	n0r = nodes[n0.right[-1]] if n0.right[-1] != None else nodes[0].left#==PAD
        
        #explicit markov features for joint POS learning
        m0 = nodes[ i-1 ] if (i > 1 and nodes[ i: ]) else nodes[0].left#==PAD
	
	#return [(nd.id, nd.form) for nd in s2r,s2l,s1r,s1l,s0r,s0l,n0l,s2,s1,s0,n0]
	return [(nd.id, nd.form, nd.ptag) for nd in s3r,s3l,s2r,s2l,s1r,s1l,s0r,s0l,n0r,n0l,s3,s2,s1,s0,m0,n0]

class Configuration(object):
    def __init__(self, nodes=[], training=True):
        self.stack = list()
        self.queue = range(len(nodes))[1:]
        self.b0 = 1
        if training:
            self.nodes = nodes[:1]+get_in_order(nodes[1:-1])+nodes[-1:]
            for tnode in range(1, len(self.nodes[1:-1])+1):
                tnodeparent = self.nodes[self.nodes[tnode].parent]
                self.nodes[tnodeparent.id] = self.nodes[tnodeparent.id]._replace(children=tnodeparent.children+[tnode])
        else:
            self.nodes = nodes

def MPC(node, parent, tree):
        if parent == -1:
            return node
        else:
            node, parent = tree[parent].id, tree[parent].pparent
            return MPC(node, parent,tree)

def MPCs(graph, swparser):
    configuration = Configuration(graph)
    while not swparser.inFinalState(configuration):
        goldTransitionFunc, goldLabel = swparser.predict(configuration)
        goldTransition = goldTransitionFunc.__name__
        goldTransitionFunc(configuration, goldLabel)

    for p in range(1,len(configuration.nodes[1:])+1):
        pN = configuration.nodes[p]
        pNParent = pN.pparent
        if pNParent == -1:
            maxProjection = pN.id
        else:
            maxProjection = MPC(pN,pNParent, configuration.nodes)
        configuration.nodes[p] = configuration.nodes[p]._replace(
                                        pdrel="__PAD__",
                                        inorder=-1,
                                        left=[None],
                                        right=[None],
                                        children=[],
                                        mpc=maxProjection)
    configuration.nodes[-1] = configuration.nodes[-1]._replace(inorder=p)
    return [cNode._replace(pparent=-1) for cNode in configuration.nodes]

def Train(sentence, rmaps, itercount):
    dy.renew_cg()
    nnparser.initialize_graph_nodes(train=True)

    # context insensitive embeddings or local embeddings
    lembs = nnparser.get_linear_embd(sentence[1:], test=False)

    # feed word vectors into biLSTM
    fw_exps = nnparser.f_init.transduce(lembs)
    bw_exps = nnparser.b_init.transduce(reversed(lembs))

    # biLSTM states
    bi_exps = [dy.concatenate([f,b]) for f,b in zip(fw_exps, reversed(bw_exps))]

    # feed biLSTM embeddings into bibiLSTM
    bi_fw_exps = nnparser.bi_f_init.transduce(bi_exps)
    bi_bw_exps = nnparser.bi_b_init.transduce(reversed(bi_exps))

    # bibiLSTM states
    bi_bi_exps = [dy.concatenate([f,b]) for f,b in zip(bi_fw_exps, reversed(bi_bw_exps))]

    #padEmbd = dy.rectify(nnparser.word2lstm * dy.concatenate([nnparser.WORDS_LOOKUP[2], \
    #                    nnparser.TAGS[nnparser.pmaps['__PAD__']]]) + nnparser.word2lstmbias )
    padEmbd = dy.rectify(dy.concatenate([nnparser.word2lstm * nnparser.WORDS_LOOKUP[2] + \
                nnparser.word2lstmbias, nnparser.TAGS[nnparser.n_tags]]))
                #nnparser.word2lstmbias, nnparser.TAGS[nnparser.pmaps['__PAD__']]]))
    configuration = Configuration(sentence)

    loss = []
    totalError = 0
    while not nnparser.inFinalState(configuration):
        rfeatures = nnparser.basefeaturesStandard(configuration.nodes, configuration.stack, configuration.b0)
        #x = dy.concatenate([bi_bi_exps[id_-1] if id_ > 0 else bi_bi_exps[-1] \
        #        if rform == "ROOT_F" else padEmbd for id_, rform in rfeatures])
        x = dy.concatenate([dy.concatenate([bi_bi_exps[id_-1],nnparser.TAGS[nnparser.n_tags \
                if ptag == "__PAD__" else nnparser.pmaps[ptag]]]) if id_ > 0 else dy.concatenate(\
                [bi_bi_exps[-1],nnparser.TAGS[nnparser.n_tags if ptag == "__PAD__" else nnparser.pmaps[ptag]]]) \
                if rform == "ROOT_F" else padEmbd for id_, rform, ptag in rfeatures])
        output = nnparser.W2*(dy.rectify(nnparser.W1*x) + nnparser.b1) + nnparser.b2

	##NOTE <Static Oracle>
    	goldTransitionFunc, goldLabel = nnparser.predict(configuration)
	goldTransition = goldTransitionFunc.__name__
	loss.append(dy.pickneglogsoftmax(output, rmaps[(goldTransition, goldLabel)]))
        prediction = np.argmax(output.npvalue())
    	validTransitions, _ = nnparser.get_valid_transitions(configuration) #{0: <bound method arceager.SHIFT>}
    	sortedPredictions = sorted(zip(output.npvalue(), range(len(output.npvalue()))), reverse=True)
        goldLabel = goldLabel
    	for score, action in sortedPredictions:
    	    transition, predictedLabel = nnparser.tdmaps[action]
    	    if transitions[transition] in validTransitions:
    	        predictedTransitionFunc = validTransitions[transitions[transition]]
                if (predictedTransitionFunc.__name__ == goldTransition) and (goldTransition == "SHIFT"): goldLabel = predictedLabel
    	        break
    	goldTransitionFunc(configuration, goldLabel)
	#</Static Oracle>
    return loss, totalError 

def nntraining(dataset=[], n_epochs=25):
    rmaps = {v: k for k, v in nnparser.tdmaps.iteritems()}
    sys.stdout.write("Started training ...\n")
    sys.stdout.write("Training Examples: %s Classes: %s Epochs: %s WDims: %s\n\n"%(len(dataset),len(nnparser.tdmaps),n_epochs,nnparser.w_dim))
    psc = 0.0
    for epoch in range(n_epochs):
    	np.random.shuffle(dataset)
    	gtotalError = gtotal = 0
    	for sentid, sentence in enumerate(dataset):
	    sentid += 1
	    csentence = copy.deepcopy(sentence)
	    loss, totalError = Train(csentence, rmaps, epoch+1)
	    gtotal += 2 * len(sentence[1:-1]) - 1
	    gtotalError += totalError
	    loss = dy.esum(loss)
	    _ = loss.scalar_value()
	    loss.backward()
            nnparser.trainer.update()
	    sys.stderr.write("Training Instances:: %s\r"%sentid)
	sys.stderr.write('\n')
	sys.stderr.flush()
        ACC, UAS, LS, LAS = Test(args.test_file)
        sys.stderr.write("POS ACC: {}%, UAS: {}%, LS: {}% and LAS: {}%\n".format(ACC, UAS, LS, LAS))
        if LAS > psc:
            sys.stderr.write('SAVE POINT %d\n' %epoch)
            psc = LAS
            if args.smodel:
                nnparser.model.save('%s.dy' %args.smodel)
        sys.stderr.write("Epoch:: %s Loss:: %s\n" % (epoch+1, 100.*gtotalError/gtotal))
        sys.stderr.flush()

def tree_eval(sentence, scores):
    for node in sentence:
        #print(node.form, node.tag, node.ptag)
        if node.tag == node.ptag:
            scores['rightTag'] += 1
        else:    
            scores['wrongTag'] += 1
        if node.parent == node.pparent:
            scores['rightAttach'] += 1
            if node.drel == node.pdrel:
                scores['rightLabeledAttach'] += 1
            else:
                scores['wrongLabeledAttach'] += 1
        else:
            scores['wrongAttach'] += 1
            scores['wrongLabeledAttach'] += 1

        if node.drel == node.pdrel:
            scores['rightLabel'] += 1
        else:
            scores['wrongLabel'] += 1
    return scores

def Test(test_file):
    with io.open(test_file, encoding='utf-8') as infptest:
        inputGenTest = re.finditer("(.*?)\n\n", infptest.read(), re.S)

    scores = defaultdict(int)
    for idx, sentence in enumerate(inputGenTest):
        dy.renew_cg()
        nnparser.initialize_graph_nodes()

        graph = list(depenencyGraph(sentence.group(1)))
        # context insensitive embeddings or local embeddings
        lembs = nnparser.get_linear_embd(graph[1:])

        # feed word vectors into biLSTM
        fw_exps = nnparser.f_init.transduce(lembs)
        bw_exps = nnparser.b_init.transduce(reversed(lembs))

        # biLSTM states
        bi_exps = [dy.concatenate([f,b]) for f,b in zip(fw_exps, reversed(bw_exps))]

        # feed biLSTM embeddings into bibiLSTM
        bi_fw_exps = nnparser.bi_f_init.transduce(bi_exps)
        bi_bw_exps = nnparser.bi_b_init.transduce(reversed(bi_exps))

        # bibiLSTM states
        bi_bi_exps = [dy.concatenate([f,b]) for f,b in zip(bi_fw_exps, reversed(bi_bw_exps))]

        #padEmbd = dy.rectify(nnparser.word2lstm * dy.concatenate([nnparser.WORDS_LOOKUP[2], \
        #                nnparser.TAGS[nnparser.pmaps['__PAD__']]]) + nnparser.word2lstmbias )
        padEmbd = dy.rectify(dy.concatenate([nnparser.word2lstm * nnparser.WORDS_LOOKUP[2] + \
                     nnparser.word2lstmbias, nnparser.TAGS[nnparser.n_tags]]))
                     #nnparser.word2lstmbias, nnparser.TAGS[nnparser.pmaps['__PAD__']]]))

        configuration = Configuration(graph, training=False)
        while not nnparser.inFinalState(configuration):
            rfeatures = nnparser.basefeaturesStandard(configuration.nodes, configuration.stack, configuration.b0)
            #x = dy.concatenate([bi_bi_exps[id_-1] if id_ > 0 else bi_bi_exps[-1] \
            #            if rform == "ROOT_F" else padEmbd for id_, rform in rfeatures])
            #x = dy.concatenate([dy.concatenate([bi_bi_exps[id_-1],nnparser.TAGS[nnparser.pmaps[ptag]]]) if id_ > 0 \
            #    else dy.concatenate([bi_bi_exps[-1],nnparser.TAGS[nnparser.pmaps[ptag]]]) \
            #    if rform == "ROOT_F" else padEmbd for id_, rform, ptag in rfeatures])
            x = dy.concatenate([dy.concatenate([bi_bi_exps[id_-1],nnparser.TAGS[nnparser.n_tags \
                    if ptag == "__PAD__" else nnparser.pmaps[ptag]]]) if id_ > 0 else dy.concatenate(\
                    [bi_bi_exps[-1],nnparser.TAGS[nnparser.n_tags if ptag == "__PAD__" else nnparser.pmaps[ptag]]]) \
                    if rform == "ROOT_F" else padEmbd for id_, rform, ptag in rfeatures])
            output = dy.softmax(nnparser.W2*(dy.rectify(nnparser.W1*x) + nnparser.b1) + nnparser.b2)
    	    validTransitions, _ = nnparser.get_valid_transitions(configuration) #{0: <bound method arceager.SHIFT>}
    	    sortedPredictions = sorted(zip(output.npvalue(), range(len(output.npvalue()))), reverse=True)
    	    for score, action in sortedPredictions:
    	        transition, predictedLabel = nnparser.tdmaps[action]
    	        if transitions[transition] in validTransitions:
    	            predictedTransitionFunc = validTransitions[transitions[transition]]
    	            predictedTransitionFunc(configuration, predictedLabel)
    	            break
        #print("\n\n")
	scores = tree_eval(configuration.nodes[1:-1], scores)
        #print("\n\n")
        sys.stderr.write("Testing Instances:: %s\r"%idx)
    sys.stderr.write('\n')

    ACC = round(100. * scores['rightTag']/(scores['rightTag']+scores['wrongTag']),2)
    UAS = round(100. * scores['rightAttach']/(scores['rightAttach']+scores['wrongAttach']),2)
    LS  = round(100. * scores['rightLabel']/(scores['rightLabel']+scores['wrongLabel']), 2)
    LAS = round(100. * scores['rightLabeledAttach']/(scores['rightLabeledAttach']+scores['wrongLabeledAttach']),2)
    
    return ACC, UAS, LS, LAS

def projective(nodes):
    """Identifies if a tree is non-projective or not."""
    for leaf1 in nodes:
        v1,v2 = sorted([int(leaf1.id), int(leaf1.parent)])
        for leaf2 in nodes:
            v3, v4 = sorted([int(leaf2.id), int(leaf2.parent)])
            if leaf1.id == leaf2.id:continue
            if (v1 < v3 < v2) and (v4 > v2): return False
    return True

def depenencyGraph(sentence):
    """Representation for dependency trees"""
    leaf=namedtuple('leaf',
            ['id','form','lemma','tag','ctag','ptag','features','parent','pparent','drel','pdrel','inorder','left','right','children','mpc'])
    PAD = leaf._make([-1,'__PAD__','__PAD__','__PAD__','__PAD__','__PAD__',defaultdict(str),-1,-1,'__PAD__','__PAD__',-1,[None],[None],[],-1])
    yield leaf._make([-1,'ROOT_F','ROOT_L','ROOT_P','ROOT_C','ROOT_P',defaultdict(str), -1, -1, '__ROOT__', '__ROOT__',-1,PAD, [None],[],0])

    for idx,node in enumerate(sentence.split("\n"),1):
        id_,form,lemma,tag,ctag,features,parent,drel = node.split("\t")[:-2]
        node = leaf._make([int(id_),form,lemma,tag,ctag,'__PAD__',features,int(parent),-1,drel,'__PAD__',-1,[None],[None],[],int(id_)])
        features = defaultdict(str)
        for feature in node.features.split("|"): 
            try:
                features[feature.split("-")[0]] = feature.split("-")[1]
            except IndexError: features[feature.split("-")[0]] = ''
        yield node._replace(features=features)
    yield leaf._make([0,'ROOT_F','ROOT_L','ROOT_P','ROOT_C','ROOT_P',defaultdict(str),-1,-1,'__ROOT__','__ROOT__',idx,[None],[None],[],idx])

def preprocessing(train_file):
    train_sents = list()
    dlabels = set()
    tdlabels = set()
    plabels = set()
    cc = Counter()
    cc['<*>'] += 1
    cc['_UNK_'] += 1
    #tdlabels.add(('SHIFT', None))
    tdlabels.add(('SWAP', None))

    with io.open(args.train_file, encoding='utf-8') as infptrain:
        inputGenTrain = re.finditer("(.*?)\n\n", infptrain.read(), re.S)

    swparser = ArcStandard()
    for sentence in inputGenTrain:
        graph = list(depenencyGraph(sentence.group(1)))
	roots = 0
        for pnode in graph[1:-1]:
             for c in pnode.form:
                 cc[c] += 1
	     plabels.add(pnode.tag)
	     dlabels.add(pnode.drel)
	     if pnode.parent == 0:
		 roots += 1
                 tdlabels.add(('LEFTARC', pnode.drel))
	     elif pnode.id < pnode.parent:
                 tdlabels.add(('LEFTARC', pnode.drel))
	     else:
                 tdlabels.add(('RIGHTARC', pnode.drel))
	if roots == 1:
            if not projective(graph[1:-1]):graph = MPCs(graph, swparser)
            train_sents.append(graph)
    #plabels.add('LS')
    plabels.add("ROOT_P")
    dlabels.add("__PAD__")
    dlabels.add("__ROOT__")
    for pl in plabels:tdlabels.add(('SHIFT', pl))
    tdmaps = dict(zip(range(len(tdlabels)), tdlabels))
    pmaps = dict(zip(plabels, range(len(plabels))))
    #dmaps = dict(zip(dlabels, range(len(dlabels))))
    c2i = dict(zip(cc.keys(), range(len(cc))))
    meta = {'tdmaps':tdmaps, 'pmaps': pmaps, 'chars':cc, 'c2i':c2i}
    return train_sents, meta

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Neural Network Parser.", description="Beam search trainer based on SGD.")
    group = parser.add_mutually_exclusive_group()
    parser.add_argument('--train', metavar='train', dest="train_file", help="<train-file>")
    parser.add_argument('--test', metavar='test', dest="test_file", help="<test-file>")
    parser.add_argument('--embeddings', metavar='embeddings', dest="wvectors",  required=True, help="<model-name>")
    parser.add_argument('--dynet-mem', type=int, dest="memory")
    parser.add_argument('--iter', type=int, dest="iter", default=25)
    parser.add_argument('--dynet-seed', type=int, dest="seed")
    group.add_argument('--save-model', dest='smodel')
    group.add_argument('--load-model', dest='lmodel')
    args = parser.parse_args()

    sys.stderr.write("Loading word vectors ...\n")
    word_model = Word2Vec.load_word2vec_format(args.wvectors, binary=True)
    sys.stderr.write("Done\n")
    sys.stderr.flush()

    transitions = {'SHIFT':0,'LEFTARC':1,'RIGHTARC':2,'SWAP':3}
    if args.lmodel and args.test_file:
        with open('%s.meta' %args.lmodel, 'rb') as fp:
            meta = pickle.load(fp)
        nnparser = NNParser(word_model, tdmaps=meta['tdmaps'], pmaps=meta['pmaps'],
                    window=16, chars=meta['chars'], c2i=meta['c2i'])
        ACC, UAS, LS, LAS = Test(args.test_file)
	import time
	testDate = time.gmtime()[:5]
	sys.stderr.write("Year:%s, Month:%s, Day:%s, Hour:%s, Minute:%s\n" % (testDate[:5]))
        sys.stderr.write("TEST-SET\nPOS ACC: {}%, UAS: {}%, LS: {}% and LAS: {}%\n".format(ACC, UAS, LS, LAS))
	sys.exit(0)
    if args.train_file:
	train_sents, meta = preprocessing(args.train_file)
        nnparser = NNParser(word_model, tdmaps=meta['tdmaps'], pmaps=meta['pmaps'],
                    window=16, chars=meta['chars'], c2i=meta['c2i'])
        nntraining(dataset=train_sents,n_epochs=args.iter)
    	if args.smodel:
            with open('%s.meta' %args.smodel, 'wb') as fp:
                pickle.dump(meta, fp)
